<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRoleRequest;
use App\Models\UserRole;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class UserRoleController extends Controller
{
    /**
     * @param UserRole $userRole
     * @return View 
     */
    public function index(): View
    {
        $title = 'Akses Pengguna';
        $userRoles = UserRole::all();
        $userRole = new UserRole();
        return view('dashboard.user-roles.index', compact('title', 'userRoles', 'userRole'));
    }

    /**
     * @param UserRoleRequest $request
     * @return RedirectResponse
     */
    public function store(UserRoleRequest $request): RedirectResponse
    {
        UserRole::create($request->all());
        return back()->with('success', 'Berhasil menambahkan akses pengguna');
    }

    /**
     * @param UserRole $userRole
     * @return View
     */
    public function edit(UserRole $userRole): View
    {
        $title = 'Ubah Akses Pengguna';
        return view('dashboard.user-roles.edit', compact('title', 'userRole'));
    }

    /**
     * @param UserRole $userRole
     * @param UserRoleRequest $request
     * @return RedirectResponse
     */
    public function update(UserRole $userRole, UserRoleRequest $request): RedirectResponse
    {
        $userRole->update($request->all());
        return redirect()->route('user-roles.index')->with('success', 'Berhasil mengubah akses pengguna');
    }

    /**
     * @param UserRole $userRole
     * @return RedirectResponse
     */
    public function destroy(UserRole $userRole): RedirectResponse
    {
        $userRole->delete();
        return back()->with('success', 'Berhasil menghapus akses pengguna');
    }
}

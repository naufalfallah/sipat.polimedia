@extends('layouts.dashboard')

@section('dashboard_content')

<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">{{ $title ?? '' }}</h1>
    <div class="row">
        <div class="col-lg">
            @include('components.alert')
            <a href="" class="btn btn-primary mb-3" data-toggle="modal" data-target="#newSubMenuModal"><i class="fas fa-plus-circle"></i> Tambah Submenu</a>
            <!-- DataTales -->
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead class="text-center">
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Nama Submenu</th>
                                    <th scope="col">Menu</th>
                                    <th scope="col">Url</th>
                                    <th scope="col">Icon</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (!empty($submenus) && count($submenus))
                                    @php
                                        $no = 0;
                                    @endphp
                                    @foreach ($submenus as $sm)
                                        <tr>
                                            <th scope="row" class="text-center">{{ ++$no }}</th>
                                            <td>{{ $sm->name ?? null }}</td>
                                            <td>{{ $sm->menu->name ?? null }}</td>
                                            <td>{{ $sm->url ?? null }}</td>
                                            <td>{{ $sm->icon ?? null }}</td>
                                            @if($sm->is_active == 0)
                                                <td class="text-center">Tidak Aktif</td>
                                            @elseif($sm->is_active == 1)
                                                <td class="text-center">Aktif</td>
                                            @endif
                                            <td class="text-center">
                                                <a href="{{ route('submenus.edit', $sm->id) }}" class="btn btn-success mr-2"><i class="fas fa-edit"></i> Ubah</a>
                                                <form action="{{ route('submenus.destroy', $sm->id) }}" method="POST">
                                                    @method('DELETE')
                                                    @csrf
                                                    <button type="submit" class="btn btn-danger" onclick="return confirm('Hapus data?');"><i class="fas fa-trash-alt"></i> Hapus</a>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->

<!-- Modal -->
<div class="modal fade" id="newSubMenuModal" tabindex="-1" role="dialog" aria-labelledby="newSubMenuModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newSubMenuModalLabel">Tambah Submenu</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('submenus.store') }}" method="POST">
                @csrf
                <div class="modal-body">
                    @include('components.submenus.form')
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
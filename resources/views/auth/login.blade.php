@extends('layouts.app')

@section('content')

<main class="bg-gradient-primary vh-100">
    <div class="container">
        <!-- Outer Row -->
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg">
                                <div class="p-5">
                                    <div class="text-center">
                                        <!-- <hr class="sidebar-divider my-1">
                                        <h5 class="m-0 font-weight-bold text-dark">Halaman Masuk</h5>
                                        <hr class="sidebar-divider my-1 mb-3"> -->
                                        <img src="{{ asset('assets/img/Polimedia.png') }}" alt="LPPM STMIK" width="350" class="mb-3">
                                    </div>
                                    @include('components.alert')
                                    <form class="user" action="{{ route('authenticate') }}" method="POST">
                                        @csrf
                                        <div class="form-group">
                                            <input type="text" class="form-control form-control-user @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Masukkan alamat email">
                                            @error('email')
                                                <div class="invalid-feedback">
                                                   {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control form-control-user @error('password') is-invalid @enderror" name="password" placeholder="Masukkan kata sandi">
                                            @error('password')
                                                <small class="text-danger pl-3">
                                                    {{ $message }}
                                                </small>
                                            @enderror
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-user btn-block">Masuk</button>
                                    </form>
                                    <hr>
                                    <footer class="sticky-footer bg-white">
                                        <div class="container my-auto">
                                            <div class="copyright text-center my-auto">
                                                <span>Copyright &copy; Polimedia {{ date('Y') }}</span>
                                            </div>
                                        </div>
                                    </footer>
                                    <!-- <div class="text-center">
                                        <a class="small" href="('auth/forgotpassword')">Tidak Ingat Kata Sandi?</a>
                                    </div>
                                    <div class="text-center">
                                        <a class="small" href="('auth/registration')">Buat akun!</a>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

@endsection
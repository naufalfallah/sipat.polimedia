<?php

namespace App\Http\Controllers;

use App\Http\Requests\SubmenuRequest;
use App\Models\Menu;
use App\Models\Submenu;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class SubmenuController extends Controller
{
    /**
     * @return View
     */
    public function index(): View
    {
        $title = 'Pengaturan Submenu';
        $submenus = Submenu::all();
        $submenu = new Submenu();
        $menus = Menu::all();
        return view('dashboard.submenus.index', compact('title', 'submenus', 'submenu', 'menus'));
    }

    /**
     * @param SubmenuRequest $request
     * @return RedirectResponse
     */
    public function store(SubmenuRequest $request): RedirectResponse
    {
        Submenu::create($request->all());
        return back()->with('success', 'Berhasil menambahkan submenu');
    }

    /**
     * @param Submenu $sumenu
     * @return View
     */
    public function edit(Submenu $submenu): View
    {
        $title = 'Ubah Menu';
        $menus = Menu::all();
        return view('dashboard.submenus.edit', compact('title', 'submenu', 'menus'));
    }

    /**
     * @param Submenu $submenu
     * @param SubmenuRequest $request
     * @return RedirectResponse
     */
    public function update(Submenu $submenu, SubmenuRequest $request): RedirectResponse
    {
        $submenu->update($request->all());
        return redirect()->route('submenus.index')->with('success', 'Berhasil mengubah submenu');
    }

    /**
     * @param Submenu $submenu
     * @return RedirectResponse
     */
    public function destroy(Submenu $submenu): RedirectResponse
    {
        $submenu->delete();
        return back()->with('success', 'Berhasil menghapus submenu');
    }
}

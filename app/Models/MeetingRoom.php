<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MeetingRoom extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'image',
        'capacity',
    ];
    public $timestamps = false;
    
    /**
     * @return books
     */
    public function books()
    {
        return $this->hasMany(MeetingRoomBook::class, 'meeting_room_id');
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\MeetingRoom;
use App\Models\MeetingRoomBook;
use Illuminate\Contracts\View\View;

class DashboardController extends Controller
{
    /**
     * @return View
     */
    public function index(): View
    {
        $title = 'Dashboard';
        $meetingRooms = MeetingRoom::all();
        foreach ($meetingRooms as $meetingRoom) {
            $meetingRoom->total_book = MeetingRoomBook::where('meeting_room_id', $meetingRoom->id)->get()->count();
        }
        return view('dashboard.index', compact('title', 'meetingRooms'));
    }
}

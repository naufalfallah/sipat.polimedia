@extends('layouts.dashboard')

@section('dashboard_content')

<div class="container col-md-8">
    <div class="card shadow">
        <div class="card-header bg-primary text-light text-center">
            Form Peminjaman
        </div>
        <div class="row no-gutters">
            <div class="col-lg">
                <div class="card-body">
                    @include('components.alert')
                    <form action="{{ route('meeting-rooms.books.store', $meetingRoom->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <label for="unit" class="col-sm-3 col-form-label">Unit</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="unit" name="unit" value="{{ auth()->user()->name }}" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="event_name" class="col-sm-3 col-form-label">Nama Kegiatan</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control @error('event_name') is-invalid @enderror" id="event_name" placeholder="Nama Kegiatan" name="event_name" autocomplete="off" value="{{ old('event_name') }}">
                                @error('event_name')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="date" class="col-sm-3 col-form-label">Tanggal</label>
                            <div class="col-sm-4">
                                <input type="date" class="form-control @error('date') is-invalid @enderror" id="date" name="date" value="{{ old('date') }}">
                                @error('date')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="start_time" class="col-sm-3 col-form-label">Waktu Mulai</label>
                            <div class="col-sm-3">
                                <input type="time" class="form-control @error('start_time') is-invalid @enderror" id="start_time" name="start_time" value="{{ old('start_time') }}">
                                @error('start_time')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="end_time" class="col-sm-3 col-form-label">Waktu Selesai</label>
                            <div class="col-sm-3">
                                <input type="time" class="form-control @error('end_time') is-invalid @enderror" id="end_time" name="end_time" value="{{ old('end_time') }}">
                                @error('end_time')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="upload_file" class="col-sm-3 col-form-label">File</label>
                            <div class="col-sm-7">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input @error('upload_file') is-invalid @enderror" id="upload_file" name="upload_file">
                                    <label class="custom-file-label" for="customFile">Masukkan File</label>
                                    @error('upload_file')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success btn-lg btn-block"><i class="fas fa-save"></i> Simpan</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
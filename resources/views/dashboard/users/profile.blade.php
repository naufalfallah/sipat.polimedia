@extends('layouts.dashboard')

@section('dashboard_content')

<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">{{ $title ?? '' }}</h1>
    @include('components.alert')
    <div class="card mb-3 col-lg-8">
        <div class="row no-gutters">
            <div class="col-md-4">
                <img src="{{ asset(auth()->user()->image) }}" class="card-img">
            </div>
            <div class="col-md-8">
                <div class="card-body">
                    <h5 class="card-title">{{ auth()->user()->name }}</h5>
                    <p class="card-text">{{ auth()->user()->email }}</p>
                    <p class="card-text"><small class="text-muted">Bergabung sejak {{ date('d F Y H:i:s', strtotime(auth()->user()->created_at)) }}</small></p>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

@endsection
<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\MeetingRoomController;
use App\Http\Controllers\MeetingRoomBookController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\SubmenuController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserRoleController;
use App\Http\Controllers\UserRoleMenuController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['guest'])->group(function () {
    Route::controller(AuthController::class)->group(function () {
        Route::get('', 'login')->name('login');
        Route::post('login', 'authenticate')->name('authenticate');
    });
});
Route::middleware(['auth'])->group(function () {
    Route::controller(DashboardController::class)->group(function () {
        Route::get('dashboard', 'index')->name('dashboard');
    });
    Route::resource('menus', MenuController::class)->except('create', 'show');
    Route::resource('submenus', SubmenuController::class)->except('create', 'show');
    Route::resource('user-roles', UserRoleController::class)->except('create', 'show');
    Route::resource('user-roles.menus', UserRoleMenuController::class)->only('index', 'store', 'destroy');
    Route::controller(UserController::class)->group(function () {
        Route::get('profile', 'profile')->name('profile');
        Route::get('edit-profile', 'editProfile')->name('edit-profile');
        Route::put('update-profile', 'updateProfile')->name('update-profile');
        Route::get('change-password', 'changePassword')->name('change-password');
        Route::put('update-password', 'updatePassword')->name('update-password');
    });
    Route::resource('users', UserController::class)->except('create', 'show', 'edit');
    Route::get('meeting-rooms/{meeting_room}/json', [MeetingRoomController::class, 'listBooks'])->name('meeting-rooms.json');
    Route::resource('meeting-rooms', MeetingRoomController::class)->only('index', 'show');
    Route::resource('meeting-rooms.books', MeetingRoomBookController::class)->except('show', 'edit', 'destory');
    Route::post('logout', [AuthController::class, 'logout'])->name('logout');
});
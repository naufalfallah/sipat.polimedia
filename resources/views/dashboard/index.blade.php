@extends('layouts.dashboard')

@section('dashboard_content')

<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">{{ $title ?? '' }}</h1>
    <div class="row">
        @foreach($meetingRooms as $meetingRoom)
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-danger shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-md font-weight-bold text-primary text-uppercase mb-1"> {{ $meetingRoom->name }}</div>
                                <div class="h6 mb-0 font-weight-bold text-gray-800">Peminjam : {{ $meetingRoom->total_book }} </div>
                            </div>
                            <div class="col-auto">
                                <i class="far fa-calendar-alt fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <!-- /.container-fluid -->
</div>
<!-- End of Main Content -->

@endsection
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
    ];
    public $timestamps = false;

    /**
     * @return user
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'role_id');
    }
    
    /**
     * @return access_menus
     */
    public function access_menus()
    {
        return $this->hasMany(UserRoleMenu::class, 'role_id');
    }
}

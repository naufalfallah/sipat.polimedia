<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ url('user') }}">
        <div class="sidebar-brand-icon">
            <i class="fas fa-university"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Polimedia</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider">
    @foreach (auth()->user()->role->access_menus as $access_menu)
        <div class="sidebar-heading">
            {{ $access_menu->menu->name }}
        </div>
        @foreach($access_menu->menu->submenus as $submenu)
            @if($submenu->is_active == 1)
                <li class="nav-item @if($title == $submenu->name) active @endif">

                <!-- Nav Item - Dashboard -->
                <a class="nav-link pb-0" href="{{ url($submenu->url) }}">
                    <i class="{{ $submenu->icon }}"></i>
                    <span>{{ $submenu->name }}</span></a>
                </li>
            @endif
        @endforeach
        <!-- Divider -->
        <hr class="sidebar-divider mt-3">
    @endforeach

    <!-- Nav Item - Charts -->
    <li class="nav-item">
        <form class="nav-link px-1 pt-0" action="{{ route('logout') }}" method="POST">
            @csrf
            <button type="submit" class="btn btn-link text-white">
                <i class="fas fa-fw fa-sign-out-alt"></i>
                <span>Logout</span>
            </button>
        </form>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
</ul>
<!-- End of Sidebar -->
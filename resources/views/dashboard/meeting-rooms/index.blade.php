@extends('layouts.dashboard')

@section('dashboard_content')

<div class="container">
    @include('components.alert')
</div>
<div class="container">
    <div class="card shadow">
        <div class="card-header bg-primary text-light text-center">
            {{ $title ?? '' }}
        </div>
        <div class="row no-gutters">
            @foreach($meetingRooms as $mr)
                @if(auth()->user()->role_id == 4 && $mr->id == 1 || 
                    auth()->user()->role_id == 4 && $mr->id == 2 || 
                    auth()->user()->role_id == 4 && $mr->id == 5)
                    @php 
                        continue;
                    @endphp
                @endif
                <div class="col-sm-6">
                    <div class="card-body">
                        <div class="card mb-3" style="max-width: 540px;">
                            <div class="row no-gutters">
                                <div class="col-md-4">
                                    <img src="{{ asset($mr->image) }}" class="card-img" alt="{{ $mr->name }}">
                                </div>
                                <div class="col-md-8">
                                    <div class="card-body">
                                        <h5 class="card-title">{{ $mr->name }}</h5>
                                        <p class="card-text">Silahkan melihat jadwal terlebih dahulu, sebelum meminjam ruangan.</p>
                                        @if($mr->capacity !== null)
                                            <div class="alert alert-warning" role="alert">
                                                <b>Batas Maksimum Peserta : {{ $mr->capacity }} orang</b>
                                            </div>
                                        @endif
                                        <a href="{{ route('meeting-rooms.show', $mr->id) }}" class="btn btn-primary mr-4"><i class="far fa-calendar-alt"></i> Jadwal</a>
                                        <a href="{{ route('meeting-rooms.books.create', $mr->id) }}" class="btn btn-success"><i class="fas fa-pen"></i> Pinjam</a>
                                        @if(auth()->user()->role_id == 1)
                                            <a href="{{ route('meeting-rooms.books.index', $mr->id) }}" class="btn btn-secondary mt-3"><i class="far fa-calendar-check"></i> Data Jadwal Pinjam</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>

@endsection
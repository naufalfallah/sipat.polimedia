<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'name',
    ];
    public $timestamps = false;

    /**
     * @return access_menus
     */
    public function access_menus()
    {
        return $this->belongsTo(UserRoleMenu::class, 'menu_id');
    }
    
    /**
     * @return submenus
     */
    public function submenus()
    {
        return $this->hasMany(Submenu::class, 'menu_id');
    }
}

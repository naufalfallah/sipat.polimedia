@extends('layouts.dashboard')

@section('dashboard_content')

<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">{{ $title ?? '' }}</h1>
    <form action="{{ route('user-roles.update', $userRole->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="row">
            <div class="col-md-8">
                @include('components.user-roles.form')
            </div>
        </div>
    </form>
</div>
<!-- /.container-fluid -->

@endsection
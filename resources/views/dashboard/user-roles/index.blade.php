@extends('layouts.dashboard')

@section('dashboard_content')

<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">{{ $title ?? '' }}</h1>
    <div class="row">
        <div class="col-lg-6">
            @include('components.alert')
            <a href="" class="btn btn-primary mb-3" data-toggle="modal" data-target="#newRoleModal"><i class="fas fa-user-plus"></i> Tambah Akses</a>
            <!-- DataTales -->
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead class="text-center">
                                <tr>
                                    <th scope="col" class="text-center">No</th>
                                    <th scope="col">Akses</th>
                                    <th scope="col">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (!empty($userRoles) && count($userRoles))
                                    @php
                                        $no = 0;
                                    @endphp
                                    @foreach ($userRoles as $ur)
                                        <tr>
                                            <th scope="row" class="text-center">{{ ++$no }}</th>
                                            <td>{{ $ur->name ?? null }}</td>
                                            <td class="text-center">
                                                <div class="btn-group" role="group" aria-label="Basic example">
                                                    <a href="{{ route('user-roles.menus.index', $ur->id) }}" class="btn btn-warning"><i class="fas fa-user-cog"></i></a>
                                                    <a href="{{ route('user-roles.edit', $ur->id) }}" class="btn btn-success mr-2"><i class="fas fa-edit"></i> Ubah</a>
                                                    <form action="{{ route('user-roles.destroy', $ur->id) }}" method="POST">
                                                        @method('DELETE')
                                                        @csrf
                                                        <button type="submit" class="btn btn-danger" onclick="return confirm('Hapus data?');"><i class="fas fa-trash-alt"></i> Hapus</a>
                                                    </form>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->

<!-- Modal -->
<div class="modal fade" id="newRoleModal" tabindex="-1" role="dialog" aria-labelledby="newRoleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newRoleModalLabel">Tambah Akses Pengguna</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('user-roles.store') }}" method="POST">
                @csrf
                <div class="modal-body">
                    @include('components.user-roles.form')
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
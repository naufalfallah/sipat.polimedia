@extends('layouts.dashboard')

@section('dashboard_content')

<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">{{ $title ?? '' }}</h1>
    <h5 class="h5 text-gray-800">Role : {{ $userRole->name }}</h5>
    <div class="row">
        <div class="col-lg-6">
            @include('components.alert')
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Menu</th>
                                <th scope="col">Access</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (!empty($menus) && count($menus))
                                @php
                                    $no = 0
                                @endphp
                                @foreach ($menus as $menu)
                                <tr>
                                    <th scope="row">{{ ++$no }}</th>
                                    <td>{{ $menu->name }}</td>
                                    <td>
                                        <div class="form-check">
                                            <input class="form-check-input input-access-menu" type="checkbox" 
                                                @foreach($userRole->access_menus as $access_menu) 
                                                    @if($access_menu->menu_id == $menu->id)
                                                        data-access_menu_id="{{ $access_menu->id }}" checked
                                                    @endif
                                                @endforeach
                                                data-menu_id="{{ $menu->id }}">
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->

<script>
    $(function() {
        $('.input-access-menu').on('click', function() {
            const menu_id = $(this).data('menu_id');
            const access_menu_id = $(this).data('access_menu_id');

            if ($(this).is(':checked')) {
                $.ajax({
                    url: "{{ route('user-roles.menus.store', $userRole->id) }}",
                    type: 'POST',
                    data: {
                        '_token': '{{ csrf_token() }}',
                        'menu_id': menu_id,
                    },
                    dataType: 'json',
                    success: function(response) {
                        console.log(response.message)
                    }
                });
            } else {
                $.ajax({
                    url: "{{ url('/') }}/user-roles/{{ $userRole->id }}/menus/" + access_menu_id,
                    method: 'POST',
                    data: {
                        '_token': '{{ csrf_token() }}',
                        '_method': 'DELETE',
                    },
                    dataType: 'json',
                    success: function(response) {
                        console.log(response.message)
                    }
                });
            }
            
        });
    });
</script>

@endsection
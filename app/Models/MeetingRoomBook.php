<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MeetingRoomBook extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'meeting_room_id',
        'unit',
        'event_name',
        'date',
        'start_time',
        'end_time',
        'file',
        'status',
    ];
    
    /**
     * @return user
     */
     public function user()
     {
         return $this->belongsTo(User::class, 'user_id');
     }
    
     /**
      * @return meetingRoom
      */
      public function meetingRoom()
      {
          return $this->belongsTo(MeetingRoom::class, 'meeting_room_id');
      }
}

@if (session('success'))
	<div class="alert alert-success alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<div class="alert-message">
			<strong>{{ session('success') }}</strong>
		</div>
	</div>
@elseif (session('failed'))
	<div class="alert alert-danger alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<div class="alert-message">
			<strong>{{ session('failed') }}</strong>
		</div>
	</div>
@endif
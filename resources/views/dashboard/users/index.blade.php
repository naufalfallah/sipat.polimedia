@extends('layouts.dashboard')

@section('dashboard_content')

<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">{{ $title ?? '' }}</h1>
    <a href="" class="btn btn-primary mb-3" data-toggle="modal" data-target="#newDataUser"><i class="fas fa-user-plus"></i> Tambah Pengguna</a>
    <h5 class="h5 text-gray-800">Total Pengguna ( {{ count($users) }} )</h5>
    <div class="row">
        <div class="col-lg">
            @include('components.alert')
            <!-- DataTales -->
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead class="text-center">
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Foto</th>
                                    <th scope="col">Nama</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Tanggal Daftar</th>
                                    <th scope="col">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (!empty($users) && count($users))
                                    @php
                                        $no = 0
                                    @endphp
                                    @foreach ($users as $user)
                                        <tr>
                                            <th scope="row" class="text-center">{{ ++$no }}</th>
                                            <td><img src="{{ asset($user->image) }}" alt="{{ $user->name }}" width="70px"></td>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->email }}</td>
                                            @if($user->is_active == 0)
                                                <td class="text-center">Tidak Aktif</td>
                                            @elseif($user->is_active == 1)
                                                <td class="text-center">Aktif</td>
                                            @endif
                                            <td class="text-center">{{ date('d F Y H:i:s', strtotime($user->created_at)) }}</td>
                                            <td class="text-center">
                                                <div class="btn-group" role="group" aria-label="Basic example">
                                                    @if($user->is_active == 0)
                                                        <form action="{{ route('users.update', $user->id) }}" method="POST" class="mr-2">
                                                            @csrf
                                                            @method('PUT')
                                                            <input type="hidden" name="is_active" value="1"/>
                                                            <button type="submit" class="btn btn-secondary" onclick="return confirm('Aktifkan pengguna?');"><i class="fas fa-times-circle"></i> Nonaktif</button>
                                                        </form>
                                                    @elseif($user->is_active == 1)
                                                        <form action="{{ route('users.update', $user->id) }}" method="POST" class="mr-2">
                                                            @csrf
                                                            @method('PUT')
                                                            <input type="hidden" name="is_active" value="0"/>
                                                            <button type="submit" class="btn btn-success" onclick="return confirm('Nonaktifkan pengguna?');"><i class="fas fa-check-circle"></i> Aktif</button>
                                                        </form>
                                                    @endif
                                                    <form action="{{ route('users.destroy', $user->id) }}" method="POST">
                                                        @method('DELETE')
                                                        @csrf
                                                        <button type="submit" class="btn btn-danger" onclick="return confirm('Hapus pengguna?');"><i class="fas fa-trash-alt"></i> Hapus</a>
                                                    </form>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->

<!-- model -->
<div class="modal fade" id="newDataUser" tabindex="-1" role="dialog" aria-labelledby="newRoleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newRoleModalLabel">Tambah Pengguna</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('users.store') }}"  method="POST">
                @csrf
                <div class="modal-body">
                    @include('components.users.form')
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
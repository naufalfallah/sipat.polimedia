<div class="form-group">
    <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Nama" value="{{ old('name', $userRole->name) }}">
    @error('name')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
    @enderror
</div>

<button type="reset" class="btn btn-secondary"><i class="fas fa-times-circle"></i> Batal</button>
<button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Simpan</button>
<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    /**
     * @return View
     */
    public function index(): View
    {
        $title = 'Pengguna';
        $users = User::all();
        $user = new User();
        return view('dashboard.users.index', compact('title', 'users', 'user'));
    }

    /**
     * @param UserRequest $request
     * @return RedirectResponse
     */
    public function store(UserRequest $request): RedirectResponse
    {
        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);
        return back()->with('success', 'Berhasil menambahkan pengguna');
    }

    /**
     * @param User $user
     * @param UserRequest $request
     * @return RedirectResponse
     */
    public function update(User $user, UserRequest $request): RedirectResponse
    {
        $user->update($request->only('is_active'));
        return back()->with('success', 'Berhasil menambahkan pengguna');
    }

    /**
     * @param User $user
     * @return RedirectResponse
     */
    public function destroy(User $user): RedirectResponse
    {
        $user->delete();
        return back()->with('success', 'Berhasil menghapus pengguna');
    }

    /**
     * @return View
     */
    public function profile(): View
    {
        $title = 'Profil';
        return view('dashboard.users.profile', compact('title'));
    }

    /**
     * @return View
     */
    public function editProfile(): View
    {
        $title = 'Ubah Profil';
        return view('dashboard.users.edit-profile', compact('title'));
    }

    /**
     * @param UserRequest $request
     * @return RedirectResponse
     */
    public function updateProfile(UserRequest $request): RedirectResponse
    {
        $image = $request->file('image');
        $image_name = time() . '_' . $image->getClientOriginalName();
        $upload_path = 'storage/profile-photos/';
        $image->move($upload_path, $image_name);
        User::find(auth()->user()->id)->update([
            'image' => $upload_path . '/' . $image_name,
        ]);
        return back()->with('success', 'Berhasil mengubah profil');
    }

    /**
     * @return View
     */
    public function changePassword(): View
    {
        $title = 'Ubah Kata Sandi';
        return view('dashboard.users.change-password', compact('title'));
    }

    /**
     * @param UserRequest $request
     * @return RedirectResponse
     */
    public function updatePassword(UserRequest $request): RedirectResponse
    {
        if (!Hash::check($request->current_password, auth()->user()->password)){
            return back()->with('failed', 'Password salah');
        }
        User::find(auth()->user()->id)->update([
            'password' => Hash::make($request->password)
        ]);
        return back()->with('success', 'Berhasil mengubah password');
    }
}

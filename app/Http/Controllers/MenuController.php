<?php

namespace App\Http\Controllers;

use App\Http\Requests\MenuRequest;
use App\Models\Menu;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class MenuController extends Controller
{
    /**
     * @return View
     */
    public function index(): View
    {
        $title = 'Pengaturan Menu';
        $menus = Menu::all();
        $menu = new Menu();
        return view('dashboard.menus.index', compact('title', 'menus', 'menu'));
    }

    /**
     * @param MenuRequest $request
     * @return RedirectResponse
     */
    public function store(MenuRequest $request): RedirectResponse
    {
        Menu::create($request->all());
        return back()->with('success', 'Berhasil menambahkan menu');
    }

    /**
     * @param Menu $menu
     * @return View
     */
    public function edit(Menu $menu): View
    {
        $title = 'Ubah Menu';
        return view('dashboard.menus.edit', compact('title', 'menu'));
    }

    /**
     * @param Menu $menu
     * @param MenuRequest $request
     * @return RedirectResponse
     */
    public function update(Menu $menu, MenuRequest $request): RedirectResponse
    {
        $menu->update($request->all());
        return redirect()->route('menus.index')->with('success', 'Berhasil mengubah menu');
    }

    /**
     * @param Menu $menu
     * @return RedirectResponse
     */
    public function destroy(Menu $menu): RedirectResponse
    {
        $menu->delete();
        return back()->with('success', 'Berhasil menghapus menu');
    }
}

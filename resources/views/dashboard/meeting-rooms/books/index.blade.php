@extends('layouts.dashboard')

@section('dashboard_content')

<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Data Jadwal Peminjaman {{ $meetingRoom->nama_ruangan }}</h1>
    <div class="row">
        <div class="col-lg">
            @include('components.alert')
            <a href="{{ route('meeting-rooms.show', $meetingRoom->id) }}" class="btn btn-primary mb-3"><i class="far fa-calendar"></i> Lihat jadwal</a>
            <!-- DataTales -->
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead class="text-center">
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Unit</th>
                                    <th scope="col">Nama Kegiatan</th>
                                    <th scope="col">Tanggal</th>
                                    <th scope="col">Waktu Mulai</th>
                                    <th scope="col">Waktu Selesai</th>
                                    <th scope="col">File</th>
                                    <th scope="col">Tanggal Pengajuan</th>
                                    <th scope="col">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (!empty($meetingRoom->books) && count($meetingRoom->books))
                                    @php
                                        $no = 0;
                                    @endphp
                                    @foreach ($meetingRoom->books as $mrb)
                                        <tr>
                                            <td scope="row" class="text-center">{{ ++$no }}</td>
                                            <td>{{ $mrb->unit ?? null }}</td>
                                            <td>{{ $mrb->event_name ?? null }}</td>
                                            <td>{{ date('d F Y', strtotime($mrb->date)) }}</td>
                                            <td>{{ date('H:i:s', strtotime($mrb->start_time)) }}</td>
                                            <td>{{ date('H:i:s', strtotime($mrb->end_time)) }}</td>
                                            <td>
                                                <a href="{{ asset($mrb->file) }}" download>{{ $mrb->file ?? null }}</a>
                                            </td>
                                            <td>{{ date('d F Y H:i:s', strtotime($mrb->created_at)) }}</td>
                                            <td class="text-center">
                                                @if($mrb->status == 'Accepted')
                                                    <span class="badge bg-success text-white">Diterima</span>
                                                @elseif($mrb->status == 'Rejected')
                                                    <span class="badge bg-secondary text-white">Ditolak</span>
                                                @else
                                                    <form action="{{ route('meeting-rooms.books.update', [$meetingRoom->id, $mrb->id]) }}" method="POST" class="mb-2">
                                                        @csrf
                                                        @method('PUT')
                                                        <input type="hidden" name="status" value="Accepted">
                                                        <button type="submit" class="btn btn-primary" onclick="return confirm('Anda yakin ingin menerima?');"><i class="fas fa-check-circle"></i> Terima</button>
                                                    </form>
                                                    <form action="{{ route('meeting-rooms.books.update', [$meetingRoom->id, $mrb->id]) }}" method="POST">
                                                        @csrf
                                                        @method('PUT')
                                                        <input type="hidden" name="status" value="Rejected">
                                                        <button type="submit" class="btn btn-danger" onclick="return confirm('Anda yakin ingin menolak?');"><i class="fas fa-times-circle"></i> Tolak</button>
                                                    </form>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->

@endsection
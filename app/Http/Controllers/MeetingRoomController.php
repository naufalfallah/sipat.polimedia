<?php

namespace App\Http\Controllers;

use App\Models\MeetingRoom;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;

class MeetingRoomController extends Controller
{
    /**
     * @return View
     */
    public function index(): View
    {
        $title = 'Ruang Rapat';
        $meetingRooms = MeetingRoom::all();
        return view('dashboard.meeting-rooms.index', compact('title', 'meetingRooms'));
    }

    /**
     * @param MeetingRoom $meetingRoom
     * @return View
     */
    public function show(MeetingRoom $meetingRoom): View
    {
        $title = 'Jadwal Ruang Rapat';
        return view('dashboard.meeting-rooms.show', compact('title', 'meetingRoom'));
    }

    /**
     * @param MeetingRoom $meetingRoom
     * @return JsonResponse $meetingRoomBooks
     */
    public function listBooks(MeetingRoom $meetingRoom): JsonResponse
    {
        $meetingRoomBooks = [];
        foreach ($meetingRoom->books as $book) {
            if ($book->status == 'Accepted') {
                array_push($meetingRoomBooks, [
                    'backgroundColor' => 'yellow',
                    'borderColor' => 'yellow',
                    'title' => $book->unit . ' : ' . $book->event_name,
                    'start' => $book->date . 'T' . $book->start_time,
                    'end' => $book->date . 'T' . $book->end_time,
                ]);
            }
        }
        return response()->json($meetingRoomBooks);
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        if ($this->method() === 'POST') {
            $rules = [
                'name' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required|min:6|confirmed',
            ];
        }

        if ($this->method() === 'PUT' || $this->method() === 'PATCH') {
            if (Route::current()->getName() === 'update-profile') {
                $rules = [
                    'image' => 'required|image',
                    // 'image' => 'required|image|mimes:jpg,png,jpeg|max:2048|dimensions:min_width=100,min_height=100,max_width=500,max_height=500',
                ];
            } else if (Route::current()->getName() === 'update-password') {
                $rules = [
                    'current_password' => 'required',
                    'password' => 'required|min:6|confirmed',
                ];
            } else {
                $rules = [];
            }
        }

        return $rules;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Nama pengguna harus diisi.',
            'email.required' => 'Email pengguna harus diisi.',
            'email.email' => 'Email tidak valid.',
            'email.unique' => 'Email sudah digunakan.',
            'password.required' => 'Kata sandi pengguna harus diisi.',
            'password.min' => 'Kata sandi minimal 6 karakter.',
            'password.confirmed' => 'Konfirmasi kata sandi tidak sama.',
            'password_confirmation.required' => 'Kata sandi saat ini harus diisi.',
            'current_password.required' => 'Kata sandi saat ini harus diisi.',
            'image.required' => 'File harus diisi.',
            'image.image' => 'File harus gambar.',
        ];
    }
}

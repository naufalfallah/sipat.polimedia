<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserRoleMenu extends Model
{
    use HasFactory;

    protected $fillable = [
        'role_id',
        'menu_id',
    ];
    public $timestamps = false;
    
    /**
     * @return role
     */
     public function role()
     {
         return $this->belongsTo(UserRole::class, 'role_id');
     }
     
    /**
     * @return menu
     */
     public function menu()
     {
         return $this->hasOne(Menu::class, 'id', 'menu_id');    
     }
}

<div class="form-group">
    <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Nama" value="{{ old('name', $submenu->name) }}">
    @error('name')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
    @enderror
</div>
<div class="form-group">
    <select name="menu_id" id="menu_id" class="form-control @error('menu_id') is-invalid @enderror">
        <option value="">Pilih Menu</option>
        @foreach ($menus as $m) : ?>
            <option value="{{ $m->id }}" {{ old('menu_id', $submenu->menu_id) == $m->id ? 'selected' : '' }}>
                {{ $m->name }}
            </option>
        @endforeach
    </select>
    @error('menu_id')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
    @enderror
</div>
<div class="form-group">
    <input type="text" class="form-control @error('url') is-invalid @enderror" name="url" placeholder="Url" value="{{ old('url', $submenu->url) }}">
    @error('url')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
    @enderror
</div>
<div class="form-group">
    <input type="text" class="form-control @error('icon') is-invalid @enderror" name="icon" placeholder="Icon" value="{{ old('icon', $submenu->icon) }}">
    @error('icon')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
    @enderror
</div>
<div class="form-group">
    <div class="form-check">
        <div class="form-check">
            <input type="radio" class="form-check-input" id="is_active1" name="is_active" value="1" {{ old('is_active', $submenu->is_active) == 1 ? 'checked' : '' }}>
            <label class="form-check-label" for="is_active1">Aktif</label>
        </div>
        <div class="form-check mb-3">
            <input type="radio" class="form-check-input" id="is_active2" name="is_active" value="0" {{ old('is_active', $submenu->is_active) == 0 ? 'checked' : '' }}>
            <label class="form-check-label" for="is_active2">Tidak Aktif</label>
            @error('is_active')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>
    </div>
</div>

<button type="reset" class="btn btn-secondary"><i class="fas fa-times-circle"></i> Batal</button>
<button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Simpan</button>
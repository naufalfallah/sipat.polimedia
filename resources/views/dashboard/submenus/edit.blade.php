@extends('layouts.dashboard')

@section('dashboard_content')

<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">{{ $title ?? '' }}</h1>
    <form action="{{ route('submenus.update', $submenu->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="row">
            <div class="col-md-8">
                @include('components.submenus.form')
            </div>
        </div>
    </form>
</div>
<!-- /.container-fluid -->

@endsection
<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubmenuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'menu_id' => 'required',
            'name' => 'required',
            'url' => 'required',
            'icon' => 'required',
            'is_active' => 'required',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return[
            'menu_id.required' => 'Menu harus diisi.',
            'name.required' => 'Nama submenu harus diisi.',
            'url.required' => 'Url harus diisi.',
            'icon.required' => 'Icon harus diisi.',
            'is_active.required' => 'Status harus diisi.',
        ];
    }
}

<div class="form-group row mt-2">
    <label for="name" class="col-sm-3 col-form-label">Nama</label>
    <div class="col-sm-9">
        <input type="text" class="form-control @error('name') is-invalid @enderror" placeholder="Nama" name="name" value="{{ old('name') }}" autocomplete="off">
        @error('name')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
        @enderror
    </div>
</div>
<div class="form-group row mt-2">
    <label for="email" class="col-sm-3 col-form-label">Email</label>
    <div class="col-sm-9">
        <input type="text" class="form-control @error('email') is-invalid @enderror" placeholder="Email" name="email" value="{{ old('email') }}" autocomplete="off">
        @error('email')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
        @enderror
    </div>
</div>
<div class="form-group row mt-2">
    <label for="password" class="col-sm-3 col-form-label">Kata Sandi</label>
    <div class="col-sm-9">
        <input type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Kata Sandi" name="password" autocomplete="off">
        @error('password')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
        @enderror
    </div>
</div>
<div class="form-group row mt-2">
    <label for="password_confirmation" class="col-sm-3 col-form-label">Konfirmasi</label>
    <div class="col-sm-9">
        <input type="password" class="form-control" placeholder="Konfirmasi Kata Sandi" name="password_confirmation" autocomplete="off">
    </div>
</div>

<button type="reset" class="btn btn-secondary"><i class="fas fa-times-circle"></i> Batal</button>
<button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Simpan</button>
<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MeetingRoomBookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        if ($this->method() === 'POST') {
            $rules = [
                // 'meeting_room_id' => 'required',
                // 'user_id' => 'required',
                'unit' => 'required',
                'event_name' => 'required',
                'date' => 'required',
                'start_time' => 'required',
                'end_time' => 'required',
                // 'file' => 'required',
            ];
        }

        return $rules;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return[
            // 'meeting_room_id.required' => 'Ruangan harus diisi.',
            // 'user_id.required' => 'Pengguna harus diisi.',
            'unit.required' => 'Unit harus diisi.',
            'event_name.required' => 'Nama kegiatan harus diisi.',
            'date.required' => 'Tanggal harus diisi.',
            'start_time.required' => 'Waktu mulai harus diisi.',
            'end_time.required' => 'Waktu selesai harus diisi.',
            // 'file.required' => 'File harus diisi.',
        ];
    }
}

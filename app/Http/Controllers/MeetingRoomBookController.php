<?php

namespace App\Http\Controllers;

use App\Http\Requests\MeetingRoomBookRequest;
use App\Models\MeetingRoom;
use App\Models\MeetingRoomBook;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class MeetingRoomBookController extends Controller
{
    /**
     * @param MeetingRoom $meetingRoom
     * @return View
     */
    public function index(MeetingRoom $meetingRoom): View
    {
        $title = 'Peminjaman Ruang Rapat';
        return view('dashboard.meeting-rooms.books.index', compact('title', 'meetingRoom'));
    }

    /**
     * @param MeetingRoom $meetingRoom
     * @return View
     */
    public function create(MeetingRoom $meetingRoom): View
    {
        $title = 'Pinjam Ruang Rapat';
        return view('dashboard.meeting-rooms.books.create', compact('title', 'meetingRoom'));
    }

    /**
     * @param MeetingRoom $meetingRoom
     * @param MeetingRoomBookRequest $request
     * @return RedirectResponse
     */
    public function store(MeetingRoom $meetingRoom, MeetingRoomBookRequest $request): RedirectResponse
    {
        $file = $request->file('upload_file');
        $fileName = time() . '_' . $file->getClientOriginalName();
        $uploadPath = 'storage/file';
        $file->move($uploadPath, $fileName);
        $request->merge([
            'meeting_room_id' => $meetingRoom->id,
            'user_id' => auth()->user()->id,
            'file' => $uploadPath . '/' . $fileName,
        ]);
        MeetingRoomBook::create($request->all());
        return redirect()->route('meeting-rooms.index')->with('success', 'Berhasil menambahkan peminjaman ruang rapat');
    }

    /**
     * @param $meeting_room_id
     * @param $meeting_room_book_id
     * @param MeetingRoomBookRequest $request
     * @return RedirectResponse
     */
    public function update($meeting_room_id, $meeting_room_book_id, MeetingRoomBookRequest $request)
    {
        $check = $this->check($meeting_room_book_id);
        if (!$check) {
            return back()->with('failed', 'Sudah ada jadwal pada jam tersebut');
        }
        MeetingRoomBook::find($meeting_room_book_id)->update($request->all());
        return back()->with('success', 'Berhasil mengubah peminjaman jadwal rapat');
    }

    /**
     * @param $meeting_room_book_id
     * @return Boolean
     */
    public function check($meeting_room_book_id)
    {
        $meetingRoomBook = MeetingRoomBook::find($meeting_room_book_id);
        $bookingStartTime = date('H:i:s', strtotime($meetingRoomBook->start_time));
        $bookingEndTime = date('H:i:s', strtotime($meetingRoomBook->end_time));

        $todayMeetingRoomBooks = MeetingRoomBook::where([
            'meeting_room_id' => $meetingRoomBook->meeting_room_id, 
            'date' => $meetingRoomBook->date, 
            'status' => 'Accepted',
        ])->get();
        foreach ($todayMeetingRoomBooks as $tmrb) {
            $listedStartTime = date('H:i:s', strtotime($tmrb->start_time));
            $listedEndTime = date('H:i:s', strtotime($tmrb->end_time));
            
            if ($bookingStartTime >= $listedStartTime && $bookingStartTime < $listedEndTime) {
                return false;
            }
            if ($bookingEndTime > $listedStartTime && $bookingEndTime <= $listedEndTime) {
                return false;
            }
            if ($bookingStartTime <= $listedStartTime && $bookingEndTime >= $listedEndTime) {
                return false;
            }
        }
        return true;
    }
}

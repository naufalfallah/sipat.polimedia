@extends('layouts.dashboard')

@section('dashboard_content')

<div class="container">
    <div class="alert alert-primary text-center mb-3" role="alert">
        <h2>Jadwal {{ $meetingRoom->name }}</h2>
    </div>
    @if(auth()->user()->role_id == '1')
        <div class="d-flex justify-content-between">
            <a href="{{ route('meeting-rooms.books.index', $meetingRoom->id) }}" class="btn btn-primary mb-3"><i class="far fa-calendar"></i> Tindak lanjut jadwal</a>
        </div>
    @endif
    <div id="calendar" class="mb-3"></div>
</div>

@endsection
@extends('layouts.app')

@section('content')

<div id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
        @include('components.sidebar')
        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content" class="d-flex flex-column">
                @include('components.topbar')
                
                <!-- Begin Page Content -->
                @yield('dashboard_content')
                
                @include('components.footer')
            </div>
            <!-- End of Main Content -->
        </div>
        <!-- End of Content Wrapper -->
    </div>
    <!-- End of Page Wrapper -->
    
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
    
    <!-- Logout Modal-->
    @include('components.logout-modal')

</div>

@endsection
<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRoleMenuRequest;
use App\Models\Menu;
use App\Models\UserRole;
use App\Models\UserRoleMenu;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;

class UserRoleMenuController extends Controller
{
    /**
     * @param UserRole $userRole
     * @return View 
     */
    public function index(UserRole $userRole): View
    {
        $title = 'Menu Akses Pengguna';
        $menus = Menu::all();
        return view('dashboard.user-roles.menus.index', compact('title', 'userRole', 'menus'));
    }

    /**
     * @param UserRole $userRole
     * @param UserRoleMenuRequest $request
     * @return RedirectResponse
     */
    public function store(UserRole $userRole, UserRoleMenuRequest $request): JsonResponse
    {
        $request->merge([
            'role_id' => $userRole->id,
        ]);
        UserRoleMenu::create($request->all());
        return response()->json(['message' => 'Berhasil menambahkan menu akses pengguna']);
    }

    /**
     * @param $user_role_id
     * @param $user_role_menu_id
     * @return RedirectResponse
     */
    public function destroy($user_role_id, $user_role_menu_id): JsonResponse
    {
        UserRoleMenu::find($user_role_menu_id)->delete();
        return response()->json(['message' => 'Berhasil menghapus menu akses pengguna']);
    }
}
